# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
import collection
import configuration


def register():
    Pool.register(
        configuration.Configuration,
        collection.Procedure,
        collection.Level,
        collection.Tracking,
        collection.Collection,
        collection.CreateCollectionStart,
        collection.Voucher,
        collection.TrackingReportStart,
        # collection.ProcessCollectionStart,
        # collection.PartyCollectionProcedure,
        module='collection', type_='model')
    Pool.register(
        collection.CreateCollection,
        collection.TrackingReportWizard,
        # collection.ProcessCollection,
        module='collection', type_='wizard')
    Pool.register(
        collection.TrackingReport,
        module='collection', type_='report')
