# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.pyson import Eval, If

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Collection Configuration'
    __name__ = 'collection.configuration'
    tracking_days_expired = fields.Integer('Days to Expired for tranckins', required=True)
