# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from collections import defaultdict
import datetime

from sql import Null
from trytond.model import Model, ModelView, ModelSQL, fields, Unique, \
    sequence_ordered
from trytond.pyson import If, Eval, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, Button, StateAction, StateReport, StateTransition
from trytond.report import Report


__all__ = ['Procedure', 'Level', 'collection',
    'CreateCollectionStart', 'CreateCollection', 'Tracking', 'Voucher', 'TrackingReportStart', 'TrackingReportWizard', 'TrackingReport']
    # 'ProcessCollectionStart', 'ProcessCollection']


class Procedure(ModelSQL, ModelView):
    'Collection Procedure'
    __name__ = 'collection.procedure'
    name = fields.Char('Name', required=True, translate=True,
        help="The main identifier of the Collection Procedure.")
    levels = fields.One2Many('collection.level', 'procedure', 'Levels')


class Level(sequence_ordered(), ModelSQL, ModelView):
    'Collection Level'
    __name__ = 'collection.level'
    procedure = fields.Many2One('collection.procedure', 'Procedure',
        required=True, select=True)
    name = fields.Char('Name')
    collect_days = fields.Numeric('Days Collect')

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        sql_table = cls.__table__()

        super(Level, cls).__register__(module_name)

        # Migration from 4.0: change days into timedelta overdue
        if table.column_exist('days'):
            cursor.execute(*sql_table.select(sql_table.id, sql_table.days,
                    where=sql_table.days != Null))
            for id_, days in cursor.fetchall():
                overdue = datetime.timedelta(days)
                cursor.execute(*sql_table.update(
                        [sql_table.overdue],
                        [overdue],
                        where=sql_table.id == id_))
            table.drop_column('days')

    def get_rec_name(self, name):
        return self.name

    def test(self, line, date):
        if self.collect_days is not None:
            return int((date - line.maturity_date).days) >= self.collect_days

_STATES = {
    'readonly': Eval('state') == 'done',
    }
_DEPENDS = ['state']

_TYPES = [
    ('phone', 'Phone'),
    ('mobile', 'Mobile'),
    ('email', 'E-Mail'),
    ('website', 'Website'),
    ('skype', 'Skype'),
    ('whatsapp', 'Whatsapp'),
    ('msm', 'MSM'),
    ('other', 'Other'),
]


class Tracking(ModelSQL, ModelView):
    'Tracking'
    __name__ = 'collection.tracking'
    _rec_name = 'name'
    collection = fields.Many2One('collection.collection', 'Collection',
        required=True,)
    date = fields.Date('Date')
    user = fields.Many2One('res.user', 'User', readonly=True)
    contact_method = fields.Selection(_TYPES, 'Contact Method', required=True,)
    debt_to_day = fields.Numeric('Debt to Day', readonly=True, states={'readonly': Eval('state') != 'active', }, depends=_DEPENDS)
    parcial_payment = fields.Function(fields.Boolean('Parcial Payment', readonly=True), 'get_parcial_payment')
    compromise_payment_date = fields.Date('Compromise Payment Day', states={'readonly': Eval('state') != 'active', }, depends=_DEPENDS)
    compromise_payment_amount = fields.Numeric('Compromise Payment',)
    customer_comments = fields.Text('Comments', states={'readonly': Eval('state') != 'active',}, depends=_DEPENDS)
    state = fields.Function(fields.Selection([
            ('done', 'Done'),
            ('active', 'Active'),
            ('inactive', "Inactive"),
            ], 'State', readonly=True), 'get_state', )
    voucher_line = fields.Many2One('account.voucher.line', 'Voucher Line', readonly=True)
    collection_amount = fields.Function(fields.Numeric('Collection Amount', readonly=True), 'get_collection_amount')
    collection_percent = fields.Function(fields.Numeric('Collection Percent', digits=(2, 2), readonly=True), 'get_collection_percent')


    @classmethod
    def __setup__(cls):
        super(Tracking, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
        ]
        # cls.amount_parcial.states['required'] = ~Bool(Eval('parcial_payment'))

    @staticmethod
    def default_user():
        return Transaction().user

    @staticmethod
    def default_contact_method():
        return 'phone'

    @staticmethod
    def default_state():
        return 'active'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def create(cls, vals):
        Collection = Pool().get('collection.collection')
        for v in vals:
            if v.get('collection'):
                collection = Collection(v['collection'])
                v['debt_to_day'] = collection.pending_payment
        super(Tracking, cls).create(vals)

    def get_collection_amount(self, name):
        if self.voucher_line and self.voucher_line.voucher.move:
            return self.voucher_line.amount
        else:
            return 0

    def get_collection_percent(self, name):
        if self.debt_to_day and self.collection_amount:
            return abs(round((self.collection_amount / self.debt_to_day), 2))

    def get_state(self, name):
        pool = Pool()
        Configuration = pool.get('collection.configuration')
        Date = pool.get('ir.date')
        configuration = Configuration(1)
        if configuration:
            _date = self.date + datetime.timedelta(days=configuration.tracking_days_expired)
            if _date > Date.today():
                return 'active'
            if self.raise_amount and self.raise_amount > 0:
                return 'done'
            else:
                return 'inactive'


    def get_parcial_payment(self, name):
        if self.compromise_payment_amount and self.debt_to_day:
            val = self.debt_to_day - self.compromise_payment_amount
            if val > 0:
                return True
            else:
                return False


class Collection(ModelSQL, ModelView):
    'Collection'
    __name__ = 'collection.collection'
    company = fields.Many2One('company.company', 'Company', required=True,
        help="Make the collection belong to the company.",
        select=True, domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        states=_STATES, depends=_DEPENDS)
    line = fields.Many2One('account.move.line', 'Move Line', required=True,
        help="The receivable line to dun for.",
        domain=[
            ('account.kind', '=', 'receivable'),
            ('account.company', '=', Eval('company', -1)),
            ['OR',
                ('debit', '>', 0),
                ('credit', '<', 0),
                ],
            ],
        states=_STATES, depends=_DEPENDS + ['company'])
    procedure = fields.Many2One('collection.procedure', 'Procedure',
        states=_STATES, depends=_DEPENDS)
    level = fields.Function(fields.Many2One('collection.level', 'Level', readonly=True),'get_level')
    # level = fields.Many2One('collection.level', 'Level',
    #     domain=[
    #         ('procedure', '=', Eval('procedure', -1)),
    #         ],
    #     states=_STATES, depends=_DEPENDS + ['procedure'])
    # blocked = fields.Boolean('Blocked',
    #     help="Check to block further levels of the procedure.")
    active = fields.Function(fields.Boolean('Active'), 'get_active',
    searcher='search_active')

    state = fields.Selection([
            ('running', "Running"),
            ('done', "Done"),
            ('cancel', "Cancel"),
            ], 'State', readonly=True)
    party = fields.Many2One('party.party', 'Party')
    invoice = fields.Function(fields.Many2One('account.invoice', 'Invoice'),
        'get_invoice')
    amount = fields.Function(fields.Numeric('Amount Total',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_amount')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
        'get_line_field')
    maturity_date = fields.Function(fields.Date('Maturity Date'),
        'get_line_field', searcher='search_line_field')
    expired_days = fields.Function(fields.Numeric('Expired Days'),
        'get_expired_days')
    total_payment = fields.Function(fields.Numeric('Total  Payment'),
        'get_total_payment')
    pending_payment = fields.Function(fields.Numeric('Pending  Payment'),
        'get_pending_payment')
    payments = fields.Function(fields.One2Many('account.move.line', None,
            'Payments'), 'get_payments')
    tracking = fields.One2Many('collection.tracking', 'collection', 'Tracking',)
    amount_second_currency = fields.Function(fields.Numeric(
            'Amount Second Currency',
            digits=(16, Eval('second_currency_digits', 2)),
            depends=['second_currency_digits']), 'get_amount_second_currency')
    second_currency = fields.Function(fields.Many2One('currency.currency',
            'Second Currency'), 'get_second_currency')
    second_currency_digits = fields.Function(fields.Integer(
            'Second Currency Digits'), 'get_second_currency_digits')
    collection_percent = fields.Function(fields.Numeric('Collection Percent', digits=(2, 2), readonly=True), 'get_collection_percent')

    @classmethod
    def __setup__(cls):
        super(Collection, cls).__setup__()
        table = cls.__table__()
        # cls._sql_constraints = [
        #     ('line_unique', Unique(table, table.line),
        #         'Line can be used only once on Collection.'),
        #     ]
        # cls._active_field = 'active'

    # @classmethod
    # def __register__(cls, module):
    #     Collection = cls.__table__()
    #     super().__register__(module)
    #     cursor = Transaction().connection.cursor()
    #
    #     # Migration from 4.8: rename done state into waiting
    #     cursor.execute(*Collection.update(
    #             [Collection.state], ['waiting'],
    #             where=Collection.state == 'done'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'running'

    # @fields.depends('state', 'active')
    # def on_change_active(self, name=None):
    #     if not self.active:
    #         self.state = 'done'

    # @classmethod
    # def validate(cls, records):
    #     for val in records:
    #         if not val.active:
    #             cls.write([val], {'state': 'done',})

    # def get_state(self, name):
    #     if self.active:
    #         return 'running'
    #     else:
    #         return 'done'

    def get_active(self, name):
        return not self.line.reconciliation

    def get_level(self, name):
        if self.procedure and self.procedure.levels and self.expired_days:
            level_ids = []
            level_ids.append(None)
            num = 0
            for l in self.procedure.levels:
                if num >= 1:
                    if l.collect_days <= self.expired_days:
                        if level_ids[0].collect_days <= l.collect_days:
                            level_ids[0] = l
                else:
                    level_ids[0] = l
                num += 1
            return level_ids[0].id

    def get_line_field(self, name):
        value = getattr(self.line, name)
        if isinstance(value, Model):
            return value.id
        else:
            return value

    def get_expired_days(self, name):
        if self.maturity_date and self.pending_payment != 0:
            Date = Pool().get('ir.date')
            return int((Date.today() - self.maturity_date).days)
        else:
            return None

    def get_total_payment(self, name):
        if self.line and self.line.origin:
            return int(sum([pay_line.amount for pay_line in self.line.origin.payment_lines]))

    def get_invoice(self, name):
        if self.line and self.line.origin:
            return self.line.origin.id

    def get_pending_payment(self, name):
        return int(self.amount - abs(self.total_payment))

    def get_payments(self, name):
        if self.line and self.line.origin:
            return [l.id for l in self.line.origin.payment_lines]

    def get_collection_percent(self, name):
        if self.amount and self.total_payment:
            return abs(round((self.total_payment / self.amount), 2))


    @classmethod
    def search_line_field(cls, name, clause):
        return [('line.' + clause[0],) + tuple(clause[1:])]

    def get_amount(self, name):
        return self.line.debit - self.line.credit

    def get_amount_second_currency(self, name):
        amount = self.line.debit - self.line.credit
        if self.line.amount_second_currency:
            return self.line.amount_second_currency.copy_sign(amount)
        else:
            return amount

    def get_second_currency(self, name):
        if self.line.second_currency:
            return self.line.second_currency.id
        else:
            return self.line.account.company.currency.id

    def get_second_currency_digits(self, name):
        if self.line.second_currency:
            return self.line.second_currency.digits
        else:
            return self.line.account.company.currency.digits

    @classmethod
    def search_active(cls, name, clause):
        reverse = {
            '=': '!=',
            '!=': '=',
            }
        if clause[1] in reverse:
            if clause[2]:
                return [('line.reconciliation', clause[1], None)]
            else:
                return [('line.reconciliation', reverse[clause[1]], None)]
        else:
            return []

    @classmethod
    def _overdue_line_domain(cls, date):
        return [
            ('account.kind', '=', 'receivable'),
            # ('collections', '=', None),
            ('maturity_date', '<=', date),
            ['OR',
                ('debit', '>', 0),
                ('credit', '<', 0),
                ],
            ('party', '!=', None),
            ('reconciliation', '=', None),
            ]

    @classmethod
    def generate_collections(cls, date=None):
        pool = Pool()
        Date = pool.get('ir.date')
        MoveLine = pool.get('account.move.line')

        if date is None:
            date = Date.today()

        # set_level = defaultdict(list)
        # for collection in cls.search([
        #             ('state', '=', 'running'),
        #             # ('blocked', '=', False),
        #             ]):
        #     procedure = collection.procedure
        #     levels = procedure.levels
        #     levels = levels[levels.index(collection.level) + 1:]
        #     if levels:
        #         for level in levels:
        #             if level.test(collection.line, date):
        #                 break
        #             else:
        #                 level = collection.level
        #         if level != collection.level:
        #             set_level[level].append(collection)
        #     else:
        #         set_level[None].append(collection)
        # to_write = []
        # for level, collections in set_level.items():
        #     if level:
        #         to_write.extend((collections, {
        #                     'level': level.id,
        #                     'state': 'running',
        #                     }))
        #     else:
        #         to_write.extend((collections, {
        #                     'state': 'done',
        #                     }))
        # if to_write:
        #     cls.write(*to_write)

        lines = MoveLine.search(cls._overdue_line_domain(date))
        line_ids = [l.id for l in lines]
        lines_exist = cls.search([('line', 'in', line_ids),])
        lines_exist = [l.line.id for l in lines_exist]
        collections = (cls._get_collection(line, date) for line in lines if not line.id in lines_exist)
        cls.save([d for d in collections if d])

    @classmethod
    def _get_collection(cls, line, date):
        # procedure = line.collection_procedure
        # if not procedure:
        #     return
        # for level in procedure.levels:
        #     if level.test(line, date):
        #         break
        # else:
        #     return
        return cls(
            line=line,
            procedure=None,
            level=None,
            party=line.party,
            )

    @classmethod
    def process(cls, collections):
        cls.write([d for d in collections
                if d.state == 'draft'], {
                'state': 'running',
                })


class CreateCollectionStart(ModelView):
    'Create Collection'
    __name__ = 'collection.create.start'
    date = fields.Date('Date', required=True,
        help="Create Collection up to this date.")

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateCollection(Wizard):
    'Create Collection'
    __name__ = 'collection.create'
    start = StateView('collection.create.start',
        'collection.collection_create_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_', 'tryton-ok', default=True),
            ])
    create_ = StateAction('collection.act_collection_form')

    def do_create_(self, action):
        pool = Pool()
        collection = pool.get('collection.collection')
        collection.generate_collections(date=self.start.date)
        return action, {}

#
# class ProcessCollectionStart(ModelView):
#     'Create Collection'
#     __name__ = 'collection.process.start'
#
#
# class ProcessCollection(Wizard):
#     'Process Collection'
#     __name__ = 'collection.process'
#     start = StateView('collection.process.start',
#         'collection.collection_process_start_view_form', [
#             Button('Cancel', 'end', 'tryton-cancel'),
#             Button('Running', 'running', 'tryton-ok', default=True),
#         ])
#     process = StateTransition()
#
#     @classmethod
#     def __setup__(cls):
#         super(ProcessCollection, cls).__setup__()
#
#         # _actions is the list that define the order of each state to process
#         # after the 'process' state.
#         cls._actions = ['running']
#
#     def next_state(self, state):
#         "Return the next state for the current state"
#         try:
#             i = self._actions.index(state)
#             return self._actions[i + 1]
#         except (ValueError, IndexError):
#             return 'end'
#
#     def transition_process(self):
#         pool = Pool()
#         collection = pool.get('collection.collection')
#         collections = collection.browse(Transaction().context['active_ids'])
#         collection.process(collections)
#         return self.next_state('running')


class Voucher(metaclass=PoolMeta):
    __name__ = 'account.voucher'


    def set_state_collection(self, line):
        Tracking = Pool().get('collection.tracking')
        Collection = Pool().get('collection.collection')
        trackings = Tracking.search([
            ('voucher_line', '=', line.id),
        ])
        if trackings:
            Collection.write([trackings[0].collection], {'state': 'running'})

    @classmethod
    def cancel(cls, vouchers):
        super(Voucher, cls).cancel(vouchers)
        for voucher in vouchers:
            for line in voucher.lines:
                voucher.set_state_collection(line)

    @classmethod
    def draft(cls, vouchers):
        super(Voucher, cls).draft(vouchers)
        for voucher in vouchers:
            for line in voucher.lines:
                voucher.set_state_collection(line)


    @classmethod
    def post(cls, vouchers):
        super(Voucher, cls).post(vouchers)
        Tracking = Pool().get('collection.tracking')
        Collection = Pool().get('collection.collection')
        for voucher in vouchers:
            for line in voucher.lines:
                if not line.move_line:
                    continue
                trackings = Tracking.search([
                    ('date', '<=', voucher.date),
                    ('collection.party.id', '=', voucher.party.id),
                    ('collection.line.id', '=', line.move_line.id),
                ])
                if trackings:
                    index = len(trackings) - 1
                    if not trackings[index].voucher_line:
                        Tracking.write([trackings[index]], {'voucher_line': line.id})
                    if line.move_line.origin.state == 'paid' or line.move_line.origin.amount_to_pay == 0:
                        Collection.write([trackings[index].collection], {'state': 'done'})


class TrackingReportStart(ModelView):
    'Tracking Report Start'
    __name__ = 'collection.tracking_report.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)
    party = fields.Many2One('party.party', "party")

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class TrackingReportWizard(Wizard):
    'Tracking Report Wizard'
    __name__ = 'collection.tracking_wizard'
    start = StateView('collection.tracking_report.start',
        'collection.tracking_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('collection.tracking_report')

    def do_print_(self, action):
        party_id = None
        if self.start.party:
            party_id = self.start.party.id
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'party': party_id,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class TrackingReport(Report):
    __name__ = 'collection.tracking_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(TrackingReport, cls).get_context(records, data)
        pool = Pool()
        Company = pool.get('company.company')
        Tracking = pool.get('collection.tracking')

        tracking_filtered = [
            ('collection.company', '=', data['company']),
            ('date', '>=', data['start_date']),
            ('date', '<=', data['end_date']),
        ]

        if data['party']:
            tracking_filtered.append(
                ('collection.party.id', '=', data['party']),
            )

        trackings = Tracking.search(tracking_filtered, order=[('collection.party.name', 'ASC')])

        report_context['records'] = trackings
        report_context['company'] = Company(data['company'])
        report_context['start'] = data['start_date']
        report_context['end'] = data['end_date']

        return report_context
